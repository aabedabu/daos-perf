

## Source CVMFS (gcc-8 and boost 1.70)

source /cvmfs/sft.cern.ch/lcg/views/LCG_96python3/x86_64-centos7-gcc8-opt/setup.sh


source /etc/profile.d/daos_env.sh


export DAOS_PATH=/data/build/daos/install



export LD_LIBRARY_PATH=/data/build/daos/install/lib64/:$LD_LIBRARY_PATH




## Build and compile

mkdir build && cd build

cmake3 ../

make



## For centos 7

/opt/rh/devtoolset-8/root/usr/bin/gcc
/opt/rh/devtoolset-8/root/usr/bin/g++


## Run like this
Create a pool and a container: 
```
source /etc/profile.d/daos_env.sh
daos_agent -i -o /data/daos/daos_agent.yml

# On another terminal:
dmg -i -l olcsl-03 pool create -s 20G -n 40G -g daos -u user
dmg -i -l olcsl-03 pool list
daos cont create --pool=POOL_ID --svc=1
```

Run the application:
```
# D_LOG_MASK=ERR ./test
./main --io_size_bytes 10240  --access_pattern wite --container ABC  --pool ABC --number_events 100 --inflight_ops 128
```

