#include <iostream>
#include <fcntl.h>

#include "utilities.hpp"
#include "DAOS_API.hpp"


int main(int argc, const char** argv) {
//int main(int argc, char** argv) {


  std::cout << "Starting DAOS-perf.\n";

  auto args = getCommandLineOptions(argc, argv);

  std::cout << io_size_bytes << "\n";
  std::cout << access_pattern << "\n";
  std::cout << pool << "\n";
  std::cout << container << "\n";
  std::cout << time_sec << "\n";
  std::cout << number_events << "\n";
  std::cout << inflight_ops << "\n";


  // AAA: for now both pool and cont ids are hardcoded
  std::string pool = "2366efdf-f22a-444a-8fb6-ab92077cc501";
  std::string svcl = "1";
  std::string cont = "6491d067-5f55-499e-9950-a78f5105f6d7";
  //std::string cont = "AAAVAS6491d067-5f55-499e-9950-a78f5105f6d7";


  // AAA: even if you increase inflight_ops to a very high value
  // at some point you reach the HW limit of inflight_ops 
  // which has usually a queue depth of 128

  DAOS_cpp DAOS = DAOS_cpp(pool, svcl, cont, inflight_ops);
 
  
 
  DAOS.init();
  DAOS.connectPool();
  DAOS.openContainer();
  DAOS.openObj();
  DAOS.execute(number_events, io_size_bytes);
  DAOS.closeObj();
  DAOS.closeContainer();
  DAOS.disconnectPool();
  DAOS.release();
  
  return 0;
}


