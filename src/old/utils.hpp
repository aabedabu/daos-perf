

// Clear PageCache, dentries and inodes.
void drop_cache() {
   int fd;
   sync();
   fd = open("/proc/sys/vm/drop_caches", O_WRONLY);
   write(fd, "3", 1);
   close(fd);
}



std::string set_path(const std::string& outputPath, const std::string& fileName) {
   std::string current_exec_name = outputPath;
   current_exec_name.append("/");
   current_exec_name.append(fileName);
   return current_exec_name;
}

int parse_arguments(int argc, char** argv, args::ArgumentParser parser, ConfigurationFileReader *conf) {
   if(argc==1) {
        printf("\nCommand line arguments have not been selected\n"); 
    return 1;
   }
 
   parser.LongPrefix("");
   parser.LongSeparator("=");
   args::HelpFlag help(parser, "HELP", "Official help menu", {"--help"});
   args::ValueFlag<string> cf(parser, "", "Config file (string)", {"--config_file"}, "");
   args::ValueFlag<string> fn(parser, "", "pool id", {"--pool"}, "");

   // Path were to read/write. It can be a file or a block device. 
   // For reading the file must exits. 
   // For writing to a block device (no file system) file_size MUST be set to -1
   args::ValueFlag<string> op(parser, "", "container id", {"--container"}, "");


   args::ValueFlag<long long> fs(parser, "", "File size in kilobytes (integer)", {"--file_size"}, 1024);

   // Size of each I/O operation in bytes
   args::ValueFlag<long> bs(parser, "", "Block size in kilobytes (integer)", {"--block_size"}, 4);

   args::ValueFlag<long> thread_number(parser, "", "Number of threads (integer)", {"--thread"}, 1);


   // Time based test in seconds
    args::ValueFlag<long> et(parser, "", "Execution time in seconds (integer)", {"--exec_time"},0);

   // By default the the access pattern is write  
   args::ValueFlag<string> wr(parser, "", "Access pattern (write, read)", {"--access_pattern"}, "write");
   
    

   try {
      parser.ParseCLI(argc, argv);
   } catch (args::Help) {
      std::cout << parser;
      return 0;
   } catch (args::ParseError e) {
      std::cerr << e.what() << std::endl;
      std::cerr << parser;
      return 1;
   } catch (args::ValidationError e) {
     std::cerr << e.what() << std::endl;
     std::cerr << parser;
     return 1;
   }
 

   if (conf->readConfig(args::get(cf))==1) return 1;
   std::cout << "========> Configuration parameters \n";
   if (cf) { conf->configFile=args::get(cf); }
   std::cout << "config_file = " << conf->configFile << "\n" ; 
   if (fn) { conf->pool=args::get(fn);}
   std::cout << "pool_id = " << conf->pool << "\n" ; 
   if (op) { conf->container=args::get(op);}
   std::cout << "container_id = " << conf->container << "\n" ;
   if (fs) { conf->fileSize=args::get(fs);}
   std::cout << "file_size [KiB] = " << conf->fileSize << "\n" ;
   if (bs) { conf->blockSize=args::get(bs);}
   std::cout << "block_size [KiB] = " << conf->blockSize << "\n" ;
   if (thread_number) { conf->threadNumber=args::get(thread_number);}
   std::cout << "Number of threads = " << conf->threadNumber << "\n" ;   
   if (et) { conf->execTime=args::get(et);}
   std::cout << "exec_time [s] = " << conf->execTime << "\n" ;

   if (wr) { 
      conf->accessPattern=args::get(wr); 
      if (conf->accessPattern.compare("read") && conf->accessPattern.compare("write")) {
        throw std::runtime_error("Please insert a valid access_pattern: 'read' or 'write' \n");
      }
   }   
   std::cout << "access_pattern = " << conf->accessPattern << "\n" ;  

 

}
  
