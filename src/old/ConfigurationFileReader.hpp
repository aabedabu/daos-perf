
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <memory>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
using namespace std;

class ConfigurationFileReader {
    public:
      std::string configFile;
      std::string pool;
      std::string container;
      int fileSize;
      int blockSize;
      int execTime;
      int threadNumber;
      std::string accessPattern;

      map<std::string, std::string> properties;


     int readConfig(string configFile){
     
     initializeProperties();
     ifstream cFile (configFile);
     if (cFile.is_open())
     {
        std::string line;
	std::string part,key,value;
	properties=map<std::string, std::string>();

	  while(getline(cFile, line)){
          if (line.find("#") != 0 && !line.empty() && line.find("="))
                {
                    part = line.substr(0, line.find("#"));
                    key = part.substr(0, line.find("="));
                    value = part.substr(line.find("=") + 1);
                    properties[key]=value;
                }

        }
	
    if (properties.count("pool") > 0) pool=properties["file_name"]; 
    if (properties.count("container")>0) container=properties["output_path"];
    if (properties.count("file_size")>0) fileSize=stoi(properties["file_size"]);
    if (properties.count("block_size_KiB")>0) blockSize=stoi(properties["block_size_KiB"]);
    if (properties.count("exec_time_seconds")>0)  execTime=stoi(properties["exec_time_seconds"]);
    if (properties.count("thread_number")>0)  threadNumber=stoi(properties["thread_number"]);  
    if (properties.count("access_pattern")>0)  accessPattern=properties["access_pattern"];  

    
	return 0;
        
    }
    else {
        cerr << "Couldn't open config file for reading.\n";
	return 1;
    }

}

void overwrite(int argc,const char** argv){
int i;
int temp;
for (i=2; i<argc; i++){
temp=i;
switch(i){
	case 2: {pool=argv[2]; break;}
	case 3: {container=argv[3]; break;}
	case 4: {fileSize=stoi(argv[4]); break;}
	case 5: {blockSize=stoi(argv[5]); break;}
	case 6: {execTime=stoi(argv[7]); break;}
	case 7: {threadNumber=stoi(argv[9]); break;}
	case 8: {accessPattern=argv[10]; break;}


	}
}		
	
}

void initializeProperties()
{
  pool="";
  container="";
  fileSize=0;
  blockSize=0;
  execTime=0;
  threadNumber=1;

}
std::string get_value(const char* key)
{
    return properties[key];
}


};

