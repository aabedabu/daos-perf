#ifndef DAOSPERF_INCLUDE_DAOS_API_HPP_
#define DAOSPERF_INCLUDE_DAOS_API_HPP_


#include <stdlib.h>
#include <stdio.h>
#include <iostream>

extern "C"{
#include <daos.h>
}


class DAOS_cpp {


public:

  DAOS_cpp(std::string pool, std::string svcl, std::string cont, uint16_t inflight_ops);
  ~DAOS_cpp(){}


  int init();
  int connectPool();
  int openContainer();
  int openObj();
  int execute(int entries, size_t io_size_bytes);
  int closeObj();
  int closeContainer();
  int disconnectPool();
  int release();



private:
  std::string m_pool = "";
  std::string m_svcl = "" ;
  std::string m_cont = "" ;
  uint16_t m_inflight_ops = 1;

  uuid_t m_puuid; /** pool UUID */
  uuid_t m_cuuid; /** container UUID */
  
  int rc;

  daos_handle_t poh = DAOS_HDL_INVAL;  /** pool handle */
  daos_handle_t coh = DAOS_HDL_INVAL;  /** container handle */
  daos_handle_t oh = DAOS_HDL_INVAL;   /** object handle */
  daos_obj_id_t oid = {  1,  1 }; /** object ID */
  d_rank_list_t *svcl; /** service leader list (to be removed in 1.2) */




};

#endif // DAOSPERF_INCLUDE_DAOS_API_HPP_

