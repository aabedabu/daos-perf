#include <boost/program_options.hpp>


namespace po = boost::program_options;

// Reading command-line parameters using boost
size_t io_size_bytes;
std::string access_pattern; // either 'read' o 'write'
std::string pool;
std::string container;
int time_sec;
size_t number_events;
size_t inflight_ops;

static po::variables_map getCommandLineOptions(int argc, const char *argv[]) {
        double max_file_size_MB;
        po::options_description genericOpts("DAOS-perf generic options");
        genericOpts.add_options()("help,h", "Print help messages")
                                                                                                ("time,t", po::value<int>(&time_sec), "Time to run the benchmark in seconds")
                                                                                                ("access_pattern,p", po::value<std::string>(&access_pattern)->required(), "Access pattern (write or read)")
                                                                                                ("io_size_bytes,s", po::value<size_t>(&io_size_bytes)->required(), "Size of each I/O operation in bytes")        ("number_events,n", po::value<size_t>(&number_events)->required(), "Number of events to process")  ("inflight_ops,i", po::value<size_t>(&inflight_ops)->required(), "Maximum number of inflight operations")

                                                                                               ("pool,p", po::value<std::string>(&pool)->required(), "DAOS pool id")  ("container,c", po::value<std::string>(&container)->required(), "DAOS container id")
                                                                                                ;

        po::options_description argumentsDescription;
        argumentsDescription.add(genericOpts);
        po::variables_map parsedArguments;
        try {
                po::store(po::parse_command_line(argc, argv, argumentsDescription), parsedArguments);

                if (parsedArguments.count("help")) {
                        std::cout << argumentsDescription << "\n";
                        exit(1);
                }

                po::notify(parsedArguments);
        }
        catch (po::error &parserError) {
                std::cerr << "Invalid arguments: " << parserError.what() << std::endl << "\n";
                std::cerr << argumentsDescription << "\n";
                exit(1);
        }


        return parsedArguments;
}

