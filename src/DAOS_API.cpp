#include <iostream>
#include <stdexcept>
#include <chrono>
#include <math.h>
#include "DAOS_API.hpp"




DAOS_cpp::DAOS_cpp(std::string pool, std::string svcl, std::string cont, uint16_t inflight_ops){
  m_pool = pool;
  m_svcl = svcl;
  m_cont = cont;
  m_inflight_ops = inflight_ops; 

  std::cout << "max inflight ops: " << inflight_ops << "\n";
  
}


int DAOS_cpp::init(){
  std::cout << "Initializing DAOS.\n" ;

  try {
  rc = daos_init();
  } catch (std::exception& e) {
    std::cout << "Failed to initialize daos.\n";
    std::cout << e.what();
  }

  // Convert uuid strings to uuid_t 
  uuid_parse(m_pool.c_str(), m_puuid);
  uuid_parse(m_cont.c_str(), m_cuuid);

  std::cout << "Parsing svcl.\n";

  try {
  svcl = daos_rank_list_parse(m_svcl.c_str(), ":");
    if (svcl == NULL) {
      std::cout << "Initialization failed.\n";
      throw std::runtime_error("err = DER_NOMEM");
    }
  } catch (std::exception& e) {
    std::cerr << "Exception caught: " << e.what() << '\n';
  }  

  return rc;
}


int DAOS_cpp::connectPool() {

  std::cout << "Connecting to pool.\n";


  try {
    rc = daos_pool_connect(m_puuid, "daos_server", svcl, DAOS_PC_RW, &poh,
                               NULL, NULL);
    if (rc) {
      std::cout << "Failed to connect to pool.\n";
      throw std::runtime_error(std::string(d_errstr(rc)) );
    }
  } catch (std::exception& e) {
    std::cerr << "Exception caught: " << e.what() << '\n';
  }     
 
  return rc; 

}


int DAOS_cpp::openContainer() {

  std::cout << "Opening container.\n";

  try {
    rc = daos_cont_open(poh, m_cuuid, DAOS_COO_RW, &coh, NULL, NULL);
    if (rc) {
      std::cout << "Failed to open container.\n";
      throw std::runtime_error(std::string(d_errstr(rc)) );
    }
  } catch (std::exception& e) {
    std::cerr << "Exception caught: " << e.what() << '\n';
  }

  return rc;
}


int DAOS_cpp::openObj() {

  // Generate hardcoded object ID
  daos_obj_generate_id(&oid, 0, OC_SX, 0);

  // Open DAOS object
  std::cout << "Opening object.\n";

  try {
    rc = daos_obj_open(coh, oid, DAOS_OO_RW, &oh, NULL);
    if (rc) {
      std::cout << "Failed to open object.\n";
      throw std::runtime_error(std::string(d_errstr(rc)) );
    }
  } catch (std::exception& e) {
    std::cerr << "Exception caught: " << e.what() << '\n';
  }

  return rc;
}









// Execute a bunch of parallel kv update
int DAOS_cpp::execute(int entries, size_t io_size_bytes) {

  std::cout << "Executing IO.\n";

  char *key[entries] = { "key1", "key2", "key1000", "key1M", "key10M", "key1B", "key10B" };
  char *val[entries] = { "val1", "val2", "val1000", "val1M", "key10M", "val1B", "val10B" };



  daos_handle_t eq;
  daos_event_t ev_array[m_inflight_ops];
  daos_event_t *evp;
  int inflight = 0;
  int ret;

  // Create event queue to manage asynchronous I/Os 
  rc = daos_eq_create(&eq);
  if (rc)
        return rc;

  int elapsed_ms = 0;
  auto start = std::chrono::high_resolution_clock::now();

  // Issue actual I/Os 
  for (int i = 0; i < entries; i++) {
    if (inflight < m_inflight_ops) {
      // Haven't reached max request in flight yet 
      evp = &ev_array[inflight];
      rc = daos_event_init(evp, eq, NULL);
      if (rc)
        break;
      inflight++;
     } else {
       
       // Max request request in flight reached, wait
       // for one i/o to complete to reuse the slot      
       rc = daos_eq_poll(eq, 1, DAOS_EQ_WAIT, 1, &evp);
       if (rc < 0)
         break;
       if (rc == 0) {
         rc = -DER_IO;
         break;
       }
       // Check if completed operation failed 
       if (evp->ev_error != DER_SUCCESS) {
         rc = evp->ev_error;
         break;
       }
       evp->ev_error = 0;
     }

     // Insert kv pair 
     rc = daos_kv_put(oh, DAOS_TX_NONE, 0, key[0], io_size_bytes,
                       val[0], evp);
     //rc = daos_kv_put(oh, DAOS_TX_NONE, 0, key[0], strlenv(val[0]),
     //                  val[0], evp);
     if (rc)
       break;
  }

  elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count();

  std::cout << "elpased_ms after put: " << elapsed_ms << "\n";

  // Wait for completion of all in-flight requests 
  do {
    ret = daos_eq_poll(eq, 1, DAOS_EQ_WAIT, 1, &evp);
    if (rc == 0 && ret == 1)
      rc = evp->ev_error;
  } while (ret == 1);
    if (rc == 0 && ret < 0)
      rc = ret;
    // Destroy event queue 
    ret = daos_eq_destroy(eq, 0);
    if (ret) {
      fprintf(stderr, "failed to destroy event queue err = %s\n",
      d_errstr(rc));
      if (ret == 0)
        ret = rc;
    }
 
  elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count();

  std::cout << "elpased_ms after wait: " << elapsed_ms << "\n";
  
  std::cout << "====================" << "\n";
  std::cout << "Throughput: " <<  1000*entries*io_size_bytes / elapsed_ms / pow(2, 20) << " MiB/s \n";
  std::cout << "====================" << "\n";
 
  return ret;
}





















int DAOS_cpp::closeObj() {

  // Close DAOS object
  try {
    if (!daos_handle_is_inval(oh)) {
      std::cout << "Closing object.\n";
      rc = daos_obj_close(oh, NULL);
      if (rc) {
        std::cout << "Failed to close object.\n";
        throw std::runtime_error(std::string(d_errstr(rc)) );
      }
    } else {
      std::cout << "Object closed.\n";
    }
  } catch (std::exception& e) {
    std::cerr << "Exception caught: " << e.what() << '\n';
  }

  return rc;
}





int DAOS_cpp::closeContainer() {

  // Close DAOS container
  try {
    if (!daos_handle_is_inval(coh)) {
      std::cout << "Closing container.\n";
      rc = daos_cont_close(coh, NULL);
      if (rc) {
        std::cout << "Failed to close container.\n";
        throw std::runtime_error(std::string(d_errstr(rc)) );
      }
    } else {
      std::cout << "Container closed.\n";
    }
  } catch (std::exception& e) {
    std::cerr << "Exception caught: " << e.what() << '\n';
  }


  return rc;
}




int DAOS_cpp::disconnectPool() {

  // Close DAOS pool
  try {
    if (!daos_handle_is_inval(poh)) {
      std::cout << "Disconnecting pool.\n";
      rc = daos_pool_disconnect(poh, NULL);
      if (rc) {
        std::cout << "Failed to disconnect pool.\n";
        throw std::runtime_error(std::string(d_errstr(rc)) );
      }
    } else {
      std::cout << "Pool disconnected.\n";
    }
  } catch (std::exception& e) {
    std::cerr << "Exception caught: " << e.what() << '\n';
  }


  return rc;
}



int DAOS_cpp::release() {

  // Free up svcl
  // AAA: compilation issue because it cannot find 
  // d_rank_list_free which is in [DAOS_PATH]/include/gurt
  //if (svcl) {
  //  d_rank_list_free(svcl);
  //}

  std::cout << "Releasing resources used on DAOS cluster.\n";
  
  try {
    rc = daos_fini(poh, NULL);
    if (rc) {
      std::cout << "Failed to release the DAOS system.\n";
      throw std::runtime_error(std::string(d_errstr(rc)) );
    }
  } catch (std::exception& e) {
    std::cerr << "Exception caught: " << e.what() << '\n';
  }

  return rc;

  
}













